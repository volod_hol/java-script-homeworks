//classwork

/*  Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:
read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств.  */

function Calculator() {
    this.read = function () {
        this.a = parseInt(prompt('Введіть перше значення a (ціле число)', '0'));
        this.b = parseInt(prompt('Введіть перше значення b (ціле число)', '0'));
    };
    this.sum = function () {
        return this.a + this.b;
    };
    this.mul = function () {
        return this.a * this.b;
    };
};

let calculator = new Calculator();

calculator.read();

alert("Sum=" + calculator.sum());

alert("Mul=" + calculator.mul());

//homework

/*  Розробіть функцію-конструктор, яка буде створювати об'єкт Human (людина).
Створіть масив об'єктів і реалізуйте функцію, яка буде сортувати елементи масиву за значенням
властивості Age за зростанням або за спаданням.  */

function Human(name, age) {
    this.name = name;
    this.age = age;
};

let human_1 = new Human('Anna', 25);
let human_2 = new Human('Bob', 8);
let human_3 = new Human('Clint', 34);
let human_4 = new Human('Dilan', 12);
let human_5 = new Human('Emma', 48);


let array = [human_1, human_2, human_3, human_4, human_5];

//сортування за зростанням

array.sort((x, y) => x.age - y.age);

for (let element of array) {
    document.write(element.name + ' - ' + element.age + '<br>');
};

document.write("=========================================<br>");

//сортування за спаданням

array.sort((x, y) => y.age - x.age);

for (let element of array) {
    document.write(element.name + ' - ' + element.age + '<br>');
};

/*  Розробіть функцію-конструктор, яка буде створювати об'єкт Human (людина). Додайте на власний розсуд властивості
і методи в цей об'єкт. Подумайте, які методи і властивості слід зробити рівня екземпляра,
а які рівня функції-конструктора.  */

document.write("=========================================<br>");

/* властивості і методи рівня екземпляра */
function People1(name, age, gender, country) {
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.country = country;
    this.about = function () {
        document.write('My name is <b>' + this.name + '</b>. I`m <b>' + this.age + '</b> old.');
        document.write(' I`m from <b>' + this.country + '</b>.<br>');
    };
};

let person1 = new People1('Anna', 25, 'female', 'Australia');
person1.about();
let person2 = new People1('Bob', 8, 'male', 'Bangladesh');
person2.about();
let person3 = new People1('Clint', 34, 'male', 'Canada');
person3.about();
let person4 = new People1('Dilan', 12, 'male', 'Denmark');
person4.about();
let person5 = new People1('Emma', 48, 'female', 'Ethiopia');
person5.about();

document.write("=========================================<br>");

/* властивості і методи рівня функції-конструктора */
function People2(name, age, gender, country) {
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.country = country;
};

let person_1 = new People2('Anna', 25, 'female', 'Australia');

let person_2 = new People2('Bob', 8, 'male', 'Bangladesh');

let person_3 = new People2('Clint', 34, 'male', 'Canada');

let person_4 = new People2('Dilan', 12, 'male', 'Denmark');

let person_5 = new People2('Emma', 48, 'female', 'Ethiopia');


People2.prototype.print = function () {
    let info_person_1 = Object.values(person_1);
    let info_person_2 = Object.values(person_2);
    let info_person_3 = Object.values(person_3);
    let info_person_4 = Object.values(person_4);
    let info_person_5 = Object.values(person_5);

    document.write(info_person_1.join(' * ') + ';' + '<br>');
    document.write(info_person_2.join(' * ') + ';' + '<br>');
    document.write(info_person_3.join(' * ') + ';' + '<br>');
    document.write(info_person_4.join(' * ') + ';' + '<br>');
    document.write(info_person_5.join(' * ') + ';' + '<br>');
};

let info_people = new People2();

info_people.print();