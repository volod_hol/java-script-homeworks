// variant_01

const doc = {
    header: "",
    body: "",
    footer: "",
    data: "",
    application: {
        header: { value: "" },
        body: { value: "" },
        footer: { value: "" },
        data: { value: "" }
    },
    show: function () {

        for (element in doc) {

            if (typeof (doc[element]) == "string") {
                doc[element] = prompt("Fill in the " + element + " of the document", "");
                document.write(element + " of the document: " + doc[element] + "<br>");
            }
            else if (typeof (doc[element]) == "object") {

                for (element in doc.application) {
                    for (elementIn in doc.application[element]) {
                        doc.application[element][elementIn] = prompt("Fill in the " + element + "  of the application document", "");
                        document.write("Value in the " + element + "  of the application document: " + doc.application[element][elementIn] + "<br>");
                    };
                };

            };
        };
    }
};

doc.show();


// variant_02
/*
const doc = new Object();
doc.header = "";
doc.body = "";
doc.footer = "";
doc.data = "";
doc.application = new Object();
doc.application.header = new Object();
doc.application.header.value = "";
doc.application.body = new Object();
doc.application.body.value = "";
doc.application.footer = new Object();
doc.application.footer.value = "";
doc.application.data = new Object();
doc.application.data.value = "";
doc.info = function () {
    for (property in doc) {
        if (typeof (doc[property]) == "string") {
            doc[property] = prompt("Fill in the " + property + " of the document", "");
            document.write(property + " of the document: " + doc[property] + "<br>");
        }
        else if (typeof (doc[property]) == "object") {

            for (property in doc.application) {
                for (propertyIn in doc.application[property]) {
                    doc.application[property][propertyIn] = prompt("Fill in the " + property + "  of the application document", "");
                    document.write("Value in the " + property + "  of the application document: " + doc.application[property][propertyIn] + "<br>");
                };
            };
        };
    };
}

doc.info();
*/