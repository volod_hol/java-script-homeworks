
// Гра "Шибениця". Перетворити код згідно нового синтаксису ES6

// Створюємо масив зі словами

let words = [
    "програма",
    "макака",
    "чудовий",
    "млинець"];
// Обираємо випадкове слово
let word = words[Math.floor(Math.random() * words.length)];
// Створюємо підсумковий масив  
let answerArray = [];
for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
}
let remainingLetters = word.length;
// Ігровий цикл  
while (remainingLetters > 0) {
    // Показуємо стан гри   
    alert(answerArray.join(" "));
    // Запит варіанту відповіді    
    let guess = prompt('Вгадайте букву, або натисніть "Скасувати" для виходу з гри.');
    if (guess === null) {
        // Вихід з ігрового циклу    
        break;
    } else if (guess.length !== 1) {
        alert("Будь ласка, введіть одинарну букву.");
    } else {
        // Оновлюємо стан гри    
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            };
        };
    };
    // Кінець ігрового циклу  
};
// Відображаємо відповідь та вітаємо гравця 
alert(answerArray.join(" "));
alert("Чудово! Було загадано слово: " + word);




/*
Реализовать функцию для создания объекта "пользователь".

Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, 
соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). 
Вывести в консоль результат выполнения функции.

Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим
функционалом:
При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить
ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 →
Ikravchenko1992).
Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword()
созданного объекта.
*/

function createNewUser() {
    this.firstName = prompt("Введіть Ваше ім'я", "");
    if (this.firstName === null) {
        debugger;
    };
    while (this.firstName === "") {
        this.firstName = prompt("Введіть, будь ласка, Ваше ім'я", "");
    };
    this.lastName = prompt("Введіть Ваше прізвище", "");
    if (this.lastName === null) {
        debugger;
    };
    while (this.lastName === "") {
        this.lastName = prompt("Введіть, будь ласка, Ваше прізвище", "");
    };
    this.birthday = prompt("Введіть дату Вашого народження", "dd.mm.yyyy");
    if (this.birthday === null) {
        debugger;
    };
    if (this.birthday === "dd.mm.yyyy") {
        this.birthday = prompt("Введіть, будь ласка, дату Вашого народження", "dd.mm.yyyy");
    };
    while (this.birthday === "") {
        this.birthday = prompt("Введіть, будь ласка, дату Вашого народження", "dd.mm.yyyy");
    };
    this.getLogin = function () {
        let newLogin = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    };
    this.getAge = function () {
        return new Date().getFullYear() - this.birthday.split(".")[2]
    };
    this.getPassword = function () {
        return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.split(".")[2]}`
    };
};

let newUser = new createNewUser();

console.log(`Ваше ім'я: ${newUser.firstName}; Ваше прізвище: ${newUser.lastName};
Ваш login: ${newUser.getLogin()}; Ваш вік: ${newUser.getAge()};
Ваш згенерований пароль: ${newUser.getPassword()}`);