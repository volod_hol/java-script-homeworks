//exercise_1

alert('Давай перевіримо умову (a + b < 4)');

let a = parseFloat(prompt('Введіть числове значення змінної "a"'));
let b = parseFloat(prompt('Введіть числове значення змінної "b"'));

let result = (a + b < 4) ? "Мало" : "Багато";

alert('Результат умови (a + b < 4) : ' + result);

//exercise_2

let login = prompt('Введіть логін')

let message = (login == 'Вася') ? "Привіт!" :
    (login == 'Директор') ? "Доброго дня!" :
        (login == '') ? "Нема логіну!" : "Такого логіну не існує або введення логіну скасовано!";

alert(message);

//exercise_3

alert('Давай виконаємо розрахунки!')

let A = parseInt(prompt("Введіть перше ціле число 'A'"));
let B = parseInt(prompt("Введіть друге ціле число 'B', що більше числа 'A'"));

if (A < B) {
    let i = A;
    let summary = 0;
    let numbers = "";
    while (i < B) {
        summary += i;
        if (i % 2 !== 0) {
            numbers += " " + i;
        };
        i++;
    };
    document.write("Сума чисел у числовому проміжку від А до В = " + summary + "<br/>");
    document.write("Непарні числа у числовому проміжку від А до В : " + numbers + "<br/>");
    }
else {
    document.write("Умова А < В не виконується!" + "<br/>");
};

document.write("<br/>" + "<br/>");

//exercise_4

document.write("<p style='color: blue; line-height: 1'>");
document.write("Прямокутник" + "<br/>" + "<br/>");
for (let a = 0; a < 15; a++) {
    for (let b = 0; b < 30; b++) {
        document.write(" * ");
    };
    document.write("<br/>");
};
document.write("</p>");

document.write("<br/>");

document.write("<p style='color: red; line-height: 1'>");
document.write("Прямокутний трикутник" + "<br/>" + "<br/>");
for (let a = 0; a < 20; a++) {
    for (let b = (a + 1); b > 0; b--) {
        document.write(" * ");
    };
    document.write("<br/>");
};
document.write("</p>");

document.write("<br/>");

document.write("<p style='text-align: center; color: green; line-height: 1'>");
document.write("Рівносторонній трикутник" + "<br/>" + "<br/>");
for (let a = 0; a < 20; a++) {
    for (let b = a + 1; b > 0; b--) {
        document.write(" * ");
    };
    document.write("<br/>");
};
document.write("</p>");

document.write("<br/>");

document.write("<p style='text-align: center; color: brown; line-height: 1'>");
document.write("Ромб" + "<br/>" + "<br/>");
for (let a = 0; a < 20; a++) {
    for (let b = a + 1; b > 0; b--) {
        document.write(" * ");
    };
    document.write("<br/>");
};
for (let a = 0; a < 20; a++) {
    for (let b = a + 1; b < 20; b++) {
        document.write(" * ");
    };
    document.write("<br/>");
};
document.write("</p>");