document.querySelector(".button1").onclick = () => { // викликаємо функцію
    const btn1 = document.getElementById("button");
    let enterfield = document.createElement("input"); // створюємо поле вводу
    enterfield.classList.add("enterfield");
    btn1.after(enterfield); // додаємо поле вводу
    enterfield.setAttribute("placeholder", "Діаметр кола");
    enterfield.setAttribute("value", "");
    let btn2 = document.createElement("input"); // створюємо кнопку
    btn2.classList.add("button2");
    btn2.setAttribute("type", "button");
    btn2.setAttribute("value", "Намалювати");
    enterfield.after(btn2); // додаємо кнопку

    btn2.onclick = () => {
        document.body.innerHTML = "";
        let diameter = enterfield.value;
        for (i = 0; i < 10; i++) { // цикл для формування рядка з елементами
            let row = document.createElement("div"); // створюємо рядок-контейнер для подальшого запису в ньому елемента
            row.style.display = "flex";
            row.style.justifyContent = "center";
            document.body.after(row); // додаємо рядок-контейнер
            for (let j = 0; j < 10; j++) { // цикл для формування кількості рядків з елементами
                let circle = document.createElement("div"); // створюємо елемент у рядку-контейнері
                circle.classList.add("circle");
                circle.style.borderRadius = "50%";
                circle.style.margin = "10px";
                circle.style.boxSizing = "border-box";
                circle.style.width = `${diameter}px`;
                circle.style.height = `${diameter}px`;
                circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`; // метод для отримання випадкових кольорів
                row.append(circle); // додаємо елементи
            };
        };
        // логіка для видалення елементу по кліку
        let [...clicks] = document.querySelectorAll(".circle");
        clicks.forEach(click => { // цикл для перебору елементів
            click.onclick = () => {
                click.remove(); // видалити елемент по кліку
            };
        });
    };
};