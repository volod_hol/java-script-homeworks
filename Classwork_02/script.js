/*
1. Якщо змінна a дорівнює 10, то виведіть 'Вірно', інакше виведіть 'Неправильно'.
        <br>
*/

let a = parseInt(prompt('Давайте вгадаємо величину змінної! Введіть ціле число', ''));
let rez = a == 10 ? alert('Вірно') : alert('Неправильно');

/*
2.У змінній min лежить число від 0 до 59. Визначте, в яку чверть години потрапляє це число
        (У першу, другу, третю або четверту).
        <br>
*/

let min = parseInt(prompt("Введіть будь-яке ціле число в діапазоні від 0 до 59", ""));
if (min >= 0 && min <= 15) {
    alert('Число потрапило у першу чверть');
} else if (min <= 30) {
    alert('Число потрапило у другу чверть');
} else if (min <= 45) {
    alert('Число потрапило у третю чверть');
} else if (min <=59) {
    alert('Число потрапило у четверту чверть');
} else {
    alert('Введене число не у заданому числовому діапазоні');
};

/*
3.Змінна num може приймати 4 значення: 1, 2, 3 або 4. Якщо вона має значення '1',
        то змінну result запишемо 'зима', якщо має значення '2' – 'весна' тощо.
        Розв'яжіть завдання через switch-case.
        <br>
*/

const num = prompt("Введіть будь-яке ціле число від 1 до 4", "");

switch (num) {
    case "1":
        { alert("зима"); }
        break;
    case "2":
        { alert("весна"); }
        break;
    case "3":
        { alert("літо"); }
        break;
    case "4":
        { alert("осінь"); }
        break;
    default:
        {
            alert("Ви ввели інше числове значення");
        }
        break;
}

/*
4. Використовуючи цикли та умовні конструкції намалюйте ялинку
        <br>
*/

alert('Намалюймо ялинку!');

document.write("<br/>");

document.write("<p style='text-align: center; color: green'>");
document.write("Ялинка" + "<br/>" + "<br/>");
for (let a = 0; a < 15; a++) {
    for (let b = a + 1; b > 0; b--) {
        document.write(" * ");
    };
    document.write("<br/>");
};
for (let a = 10; a < 25; a++) {
    for (let b = a + 1; b > 0; b--) {
        document.write(" * ");
    };
    document.write("<br/>");
};
for (let a = 20; a < 30; a++) {
    for (let b = a + 1; b > 0; b--) {
        document.write(" * ");
    };
    document.write("<br/>");
};
for (let a = 0; a < 5; a++) {
    for (let b = 0; b < 5; b++) {
        document.write(" * ");
    };
    document.write("<br/>");
};
document.write("</p>");

document.write("<br/>");

/*
5. Використовуючи умовні конструкції перевірте вік користувача, якщо користувачеві буде
        від 18 до 35 років переведіть його на сайт google.com через 2 секунди,
        якщо вік користувача буде від 35 до 60 років, переведіть його на сайт
        https://www.uz.gov.ua/,
        якщо користувачеві буде до 18 років покажіть йому першу серію лунтика з ютубу
        Виконайте це завдання за допомогою if, switch, тернарний оператор
*/

const age = parseInt(prompt("Введіть свій вік", ""));

if (age < 18) {
    window.location = "https://www.youtube.com/watch?v=icdSZKq9-sM";
}
else if (age <= 35) {
    setTimeout(function () { window.location = "https://www.google.com.ua/?gws_rd=ssl"; }, 2000);
}
else if (age <= 60) {
    window.location = "https://www.uz.gov.ua/";
}
else {
    alert("Радимо звернутися до кардіолога");
};