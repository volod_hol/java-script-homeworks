
// exercise_1

let array = [];

let n = parseInt(prompt('Введіть величину масиву (ціле число): ', ""))

for (let j = 0; j < n; ++j) {
	array[j] = Math.floor(Math.random() * 100);
};

document.getElementById("row_1").innerHTML = `Заданий масив [array]: [${array.join(', ')}] </br>`;

function map(fn, array) {
	let new_array = [];
	for (let i = 0; i < array.length; i++) {
		new_array[i] = fn(array[i]);
	}
	return new_array;
};

function fn(x) {
	return (x * x / 10);
};

document.getElementById("row_2").innerHTML = `Код виконання функції fn(x): ${fn} </br> де x - кожний елемент заданого масиву [array]. </br>`;

document.getElementById("row_3").innerHTML = `Новий масив [new_array] від функції map(fn, array): [${map(fn, array)}] </br>`;

// exercise_2

let age = parseInt(prompt("Введіть свій вік", ''));

function checkAge(age) {
	return (age > 18) ? true : confirm("Батьки дозволили?");
};

document.getElementById("row_4").innerHTML = `Результат перевірки віку: ${checkAge(age)}`;
